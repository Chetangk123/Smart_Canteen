export interface ContactDetails {
    phoneNo: string,
    name: string,
    address: string
}
