export type MealPack = {
    meal_pack_id?:    string;
    meal_pack_name?:  string;
    meal_pack_price?: number;
    meal_pack_items?: MealPackItem[];
}

export type MealPackItem = {
    meal_pack_item_id?:   string;
    meal_pack_id?:        string;
    meal_id?:             string;
    meal_name?:           string;
    meal_price?:          string;
    meal_start_time?:     string;
    raw_meal_start_time?: string;
    meal_end_time?:       string;
    raw_meal_end_time?:   string;
}
