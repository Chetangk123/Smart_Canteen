import { Component, OnInit, OnDestroy } from '@angular/core';
import { ConfigService } from '../../core/services/app.config.service';
import { AppConfig } from '../../core/interfaces/appconfig';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { CoreConfig } from 'src/app/core/interfaces/coreConfig';
import { EnvService } from 'src/app/env.service';
import { ContactDetails } from 'src/app/core/interfaces/contactDetails';
import { environment } from 'src/environments/environment';
@Component({
    selector: 'app-landing',
    templateUrl: './landing.component.html',
    styles: [
        `
            #hero {
                background: linear-gradient(
                        0deg,
                        rgba(255, 255, 255, 0.2),
                        rgba(255, 255, 255, 0.2)
                    ),
                    radial-gradient(
                        77.36% 256.97% at 77.36% 57.52%,
                        #eeefaf 0%,
                        #c3e3fa 100%
                    );
                height: 700px;
                overflow: hidden;
            }

            .pricing-card:hover {
                border: 2px solid var(--cyan-200) !important;
            }

            @media screen and (min-width: 768px) {
                #hero {
                    -webkit-clip-path: ellipse(150% 87% at 93% 13%);
                    clip-path: ellipse(150% 87% at 93% 13%);
                    height: 530px;
                }
            }

            @media screen and (min-width: 1300px) {
                #hero > img {
                    position: absolute;
                    transform: scale(1.2);
                    top: 15%;
                }

                #hero > div > p {
                    max-width: 450px;
                }
            }

            @media screen and (max-width: 1300px) {
                #hero {
                    height: 600px;
                }

                #hero > img {
                    position: static;
                    transform: scale(1);
                    margin-left: auto;
                }

                #hero > div {
                    width: 100%;
                }

                #hero > div > p {
                    width: 100%;
                    max-width: 100%;
                }
            }
            .multiline {
        white-space: pre-line;
    }
        `,
    ],
})
export class LandingComponent implements OnInit, OnDestroy {
    config: AppConfig;
    public coreConfig: CoreConfig;
    public contactDetails: ContactDetails
    subscription: Subscription;
    displayLogins: boolean = false;

    constructor(public _coreEnvService: EnvService,public configService: ConfigService, public router: Router) {
        this.coreConfig = _coreEnvService.config;
        this.contactDetails = _coreEnvService.contactDetails
    }

    ngOnInit(): void {
        this.displayLogins = !environment.production;
        this.config = this.configService.config;
        this.subscription = this.configService.configUpdate$.subscribe(
            (config) => {
                this.config = config;
            }
        );
    }

    ngOnDestroy(): void {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
}
