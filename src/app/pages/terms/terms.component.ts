import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-terms',
    templateUrl: './terms.component.html',
    styleUrls: ['./terms.component.scss'],
})
export class TermsComponent implements OnInit {
    pdfSrc: any;
    displayPdf: boolean = false;
    constructor(private sanitizer: DomSanitizer, private http: HttpClient) {}

    async ngOnInit(): Promise<void> {
        this.displayPdf = false;
        this.pdfSrc = this.sanitizer.bypassSecurityTrustResourceUrl(
            'assets/PrivacyPolicy.pdf'
        );
        let reader = new FileReader();

        reader.onload = (e: any) => {
            this.pdfSrc = e.target.result;
        };
        this.pdfSrc = reader.readAsArrayBuffer(await this.getPdfFile());
        this.displayPdf = true
    }

    getPdfFile(): Promise<File> {
        const pdfPath = 'assets/PrivacyPolicy.pdf'; // Adjust the path as needed

        return this.http
            .get(pdfPath, { responseType: 'blob' })
            .toPromise()
            .then((blob: Blob) => {
                // Convert Blob to File
                const pdfFile = new File([blob], 'PrivacyPolicy.pdf', {
                    type: 'application/pdf',
                });
                return pdfFile;
            });
    }
}
