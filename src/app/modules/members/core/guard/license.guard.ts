import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth/auth.service';
import { JwtHelperService } from '@auth0/angular-jwt';
@Injectable({
  providedIn: 'root'
})
export class LicenseGuard implements CanActivate {
    constructor(public router: Router, public authService: AuthService) {}
  canActivate(
    _route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    const user = this.authService.getUser();
        const validLicense = this.authService.getLicense()
        const helper = new JwtHelperService();
        const TokenExpired = helper.isTokenExpired(user?.token) ?? false;
        if(!validLicense){
            if (!TokenExpired && user.user_role == 'MEMBER') {
                this.authService.beginSession();
                return true;
            } else {
                localStorage.clear()
                this.router.navigate(['/member/login'], {
                    queryParams: { returnUrl: state.url },
                });
                return false;
            }
        } else {
            //localStorage.clear()
                this.router.navigate(['/member/profile']);
            return false;
        }
        //"COUNTER_MEMBER_ROLE"
    }

}
