import { DatePipe } from '@angular/common';
import { Component, HostListener, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { forkJoin, map, Observable } from 'rxjs';
import { ApiService } from 'src/app/core/services/api/api.service';
import { MemberMembershipHistoryComponent } from '../reports/member-membership-history/member-membership-history.component';
import { MemberCardHistoryComponent } from '../reports/member-card-history/member-card-history.component';
import { MemberTransactionsComponent } from '../reports/member-transactions/member-transactions.component';
import { MemberLeaveHistoryComponent } from '../reports/member-leave-history/member-leave-history.component';
import { WindowRefService } from 'src/app/window-ref.service';
import { AuthService } from '../../core/services/auth/auth.service';
import { MemberService } from '../../core/services/MemberService/member.service';
import { SettingsService } from 'src/app/core/services/settings/settings.service';
import { AddMembershipComponent } from './add-membership/add-membership.component';
import { DisplayMenuComponent } from './display-menu/display-menu.component';

declare var Razorpay: any;
@Component({
    selector: 'app-member-profile',
    templateUrl: './member-profile.component.html',
    styleUrls: ['./member-profile.component.scss'],
})
export class MemberProfileComponent implements OnInit {
    cardHistoryLoading: boolean = false;
    loading: boolean = false;
    cameraDialog: boolean = false;
    transactionLoading: boolean = false;
    membershipHistoryLoading: boolean = false;
    showWalletRecharge: boolean = false;
    walletRechargeLoading: boolean = false;
    displayTransaction: boolean = false;
    cardHistory: Observable<Object>;
    membershipHistory: Observable<Object>;
    leaveHistory: Observable<Object>;
    memberData: any;
    meal_pack_id: any = -1;
    datePipe: DatePipe = new DatePipe('en-US');
    start_date: any;
    end_date: any;
    menu_url:any;
    isImage: boolean = false;
    memberTransactions: any = [];
    membershipHistoryTransactions: any = [];
    memberCardHistory: any = [];
    memberLeaveHistory: any = [];
    membershipList: any = [];
    file_data: FormData;
    settings:any
    minWalletRecharge:any
    transactionData: Observable<Object>;
    form: FormGroup = new FormGroup({
        file: new FormControl(),
    });
    selectedProduct: any;
    transactionMenu: MenuItem[] = [
        {
            label: 'View',
            icon: 'pi pi-fw pi-eye',
            command: () => this.viewDetails(),
        },
        // {
        //     separator: true,
        // },
        // {
        //     label: 'Print',
        //     icon: 'pi pi-fw pi-print',
        //     command: () => this.printDetails(this.selectedProduct),
        // },
    ];
    commonForm: FormGroup = new FormGroup({
        customer_phnumber: new FormControl(),
        customer_email: new FormControl(),
        customer_name: new FormControl(),
        amount: new FormControl('', Validators.required),
    });
    constructor(
        public apiService: ApiService,
        public authService: AuthService,
        public ref: DynamicDialogRef,
        public router: Router,
        public route: ActivatedRoute,
        public dialogService: DialogService,
        private winRef: WindowRefService,
        public messageService: MessageService,
        public memberService: MemberService,
        public settingsService:SettingsService
    ) {}

    ngOnInit(): void {
        this.fetchCounterData()
        this.memberData = this.memberService.getMemberData();
        this.cardHistoryLoading = true;
        this.settings = this.settingsService.getSettingsData()
        this.minWalletRecharge = this.settings.filter(
            (settings: any) => settings.settings_name === 'MINIMUM_WALLET_RECHARGE'
        )[0].settings_value;
        console.log(this.minWalletRecharge);
        this.commonForm.controls['amount'].addValidators(Validators.min(this.minWalletRecharge))
        this.end_date = new Date().toISOString().substring(0, 10);
        this.start_date = this.datePipe.transform(
            new Date().setDate(new Date().getDate() - 30),
            'yyyy-MM-dd'
        );
        this.membershipList = [];
        this.membershipList.push({
            meal_pack_id: '-1',
            counter_id: '',
            meal_pack_name: 'All',
        });
        this.apiService
            .getTypeRequest(`table_data/MEAL_PACK_NAME`)
            .toPromise()
            .then((result: any) => {
                this.loading = false;
                if (result.result) {
                    result.data.forEach((element) => {
                        this.membershipList.push(element);
                    });
                } else {
                    this.membershipList = [];
                }
            });
        if (this.memberData) {
            if (this.memberData?.gender) {
            } else {
                this.loadData();
            }
            this.cardHistory = this.apiService
                .getTypeRequest(
                    `table_data/CARD_UPDATE_DETAILS/${this.memberData.member_id}`
                )
                .pipe(
                    map((res: any) => {
                        this.cardHistoryLoading = false;
                        this.memberCardHistory = res.data;
                        return res.data;
                    })
                );
            this.fetchMemberTransactions();
            this.fetchMembershipHistory();
        } else {
            this.router.navigate(['../'], { relativeTo: this.route });
        }
    }

    viewDetails(){
        this.displayTransaction = true;
    }

    fetchCounterData(){
        this.apiService
            .getTypeRequest(`specific_data/COUNTER/MY_COUNTER`)
            .toPromise()
            .then((result:any)=>{
                    this.menu_url = result.data.menu_url;
                    const fileExtension = this.getFileExtension(this.menu_url);
                    if (this.isImageExtension(fileExtension)) {
                        this.isImage = true; // 'Image';
                    } else if (this.isPdfExtension(fileExtension)) {
                        this.isImage = false; // 'PDF';
                    } else {
                        this.isImage = false;
                    }

                    console.log(this.isImage);
                })
    }

    private getFileExtension(filename: string): string {
        return filename.split('.').pop()?.toLowerCase() || '';
    }

    private isImageExtension(extension: string): boolean {
        const imageExtensions = ['jpg', 'jpeg', 'png', 'gif'];
        return imageExtensions.includes(extension);
    }

    private isPdfExtension(extension: string): boolean {
        return extension === 'pdf';
    }

    displayMenu(){
        this.dialogService.open(DisplayMenuComponent,{
            data:{
                src:this.menu_url,
                isImage: this.isImage
            },
            header: `Menu`,
            styleClass: 'w-10 sm:w-10 md:w-10 lg:w-6',
        })
    }

    fetchMemberTransactions() {
        this.transactionLoading = true;
        var Data = {
            member_id: this.memberData.member_id,
            counter_id: this.memberData?.counter_id,
            txn_id: '',
            account_id: '',
            start_date: this.start_date,
            end_date: this.end_date,
        };
        this.transactionData = this.apiService
            .postTypeRequest(`transaction_data/MEMBER_TRANSACTIONS`, Data)
            .pipe(
                map((res: any) => {
                    // res.data.sort((a,b)=>Number(a.id) - Number(b.id))
                    this.memberTransactions = res.data;
                    this.transactionLoading = false;
                    return res.data;
                })
            );
    }

    fetchMembershipHistory() {
        this.membershipHistoryLoading = true;
        var Data = {
            member_id: this.memberData.member_id,
            counter_id: this.memberData?.counter_id,
            txn_id: '',
            account_id: '',
            start_date: this.start_date,
            end_date: this.end_date,
        };
        this.membershipHistory = this.apiService
            .getTypeRequest(
                `membership_data?what=ALL_MEMBERSHIPS_BY_MEMBER&member_id=${this.memberData.member_id}`
            )
            .pipe(
                map((res: any) => {
                    this.membershipHistoryTransactions = res.data;
                    this.membershipHistoryLoading = false;
                    return res.data;
                })
            );
    }

    fetchLeaveTransactions() {
        var url = `leave_data?what=ALL_LEAVES_BY_MEMBER&member_id=${this.memberData.member_id}`;
        var membershipFilter = ``;
        if (this.meal_pack_id != -1) {
            membershipFilter = `&membership_id=${this.meal_pack_id}`;
        }
        var dateFilter = ``;
        if (this.start_date != null && this.end_date != null) {
            const start_date = this.datePipe.transform(
                this.start_date,
                'dd-MM-yyyy'
            );
            const end_date = this.datePipe.transform(
                this.end_date,
                'dd-MM-yyyy'
            );
            dateFilter = `&membership_start_date=${start_date}&membership_end_date=${end_date}`;
        }
        this.leaveHistory = this.apiService
            .getTypeRequest(url + membershipFilter + dateFilter)
            .pipe(
                map((res: any) => {
                    // res.data.sort((a,b)=>Number(a.id) - Number(b.id))
                    this.memberLeaveHistory = res.data;
                    return res.data;
                })
            );
    }

    loadData() {
        this.loading = true;
        var Data = {
            member_id: this.memberData.member_id,
            txn_id: '',
            account_id: '',
            start_date: this.start_date,
            end_date: this.end_date,
        };
        this.apiService
            .getTypeRequest(`specific_data/MEMBER/${this.memberData.member_id}`)
            .toPromise()
            .then((result: any) => {
                if (result) {
                    this.memberService.setMemberData(result.data);
                    this.ngOnInit();
                }
                this.loading = false;
            });
    }

    generateMemberTransactionsPDF() {
        const start_date = this.datePipe.transform(
            this.start_date,
            'dd-MM-yyyy'
        );
        const end_date = this.datePipe.transform(this.end_date, 'dd-MM-yyyy');
        const period = `${start_date} - ${end_date}`;
        this.dialogService.open(MemberTransactionsComponent, {
            data: {
                memberData: this.memberData,
                statement_date: this.datePipe.transform(
                    new Date(),
                    'dd-MM-yyyy'
                ),
                transactions_Data: this.memberTransactions,
                period: period,
                title: 'Member Transactions',
            },
            header: `Member Transactions`,
            styleClass: 'w-10 sm:w-10 md:w-10 lg:w-6',
        });
    }
    generateMemberTransactionsExcel() {}

    generateMemberCardHistoryPDF() {
        const start_date = this.datePipe.transform(
            this.start_date,
            'dd-MM-yyyy'
        );
        const end_date = this.datePipe.transform(this.end_date, 'dd-MM-yyyy');
        const period = `${start_date} - ${end_date}`;
        this.dialogService.open(MemberCardHistoryComponent, {
            data: {
                memberData: this.memberData,
                statement_date: this.datePipe.transform(
                    new Date(),
                    'dd-MM-yyyy'
                ),
                transactions_Data: this.memberCardHistory,
                title: 'Member Card History',
            },
            header: `Member Card History`,
            styleClass: 'w-10 sm:w-10 md:w-10 lg:w-6',
        });
    }
    generateMemberCardHistoryExcel() {}

    generateMembershipHistoryPDF() {
        this.dialogService.open(MemberMembershipHistoryComponent, {
            data: {
                memberData: this.memberData,
                statement_date: this.datePipe.transform(
                    new Date(),
                    'dd-MM-yyyy'
                ),
                transactions_Data: this.membershipHistoryTransactions,
                title: 'Member Membership History',
            },
            header: `Member Membership History`,
            styleClass: 'w-10 sm:w-10 md:w-10 lg:w-6',
        });
    }
    generateMembershipHistoryExcel() {}

    generateMemberLeaveHistoryPDF() {
        const start_date = this.datePipe.transform(
            this.start_date,
            'dd-MM-yyyy'
        );
        const end_date = this.datePipe.transform(this.end_date, 'dd-MM-yyyy');
        const period = `${start_date} - ${end_date}`;
        this.dialogService.open(MemberLeaveHistoryComponent, {
            data: {
                memberData: this.memberData,
                statement_date: this.datePipe.transform(
                    new Date(),
                    'dd-MM-yyyy'
                ),
                transactions_Data: this.memberLeaveHistory,
                title: 'Member Leave History',
            },
            header: `Member Leave History`,
            styleClass: 'w-10 sm:w-10 md:w-10 lg:w-6',
        });
    }
    generateMemberLeaveHistoryExcel() {}

    openTransactionDialog() {
        var payload = {
            customer_phnumber: this.memberData.parents_ph,
            customer_email: this.memberData.email,
            customer_name: this.memberData.full_name,
            amount: 100,
        };
        this.commonForm.patchValue(payload);
        this.showWalletRecharge = true;
        // this.walletRechargeLoading = true;
    }
    rechargeWallet() {
        if (this.commonForm.valid) {
            this.showWalletRecharge = false;
            this.apiService
                .postTypeRequest('pg_order/phonepe/wallet', this.commonForm.value)
                .toPromise()
                .then((response: any) => {
                    debugger
                    if (response.result) {
                        var options = response.data;
                        options.image = this.authService.getUser().dp_location;
                        const self = this;
                        options.handler = function (response) {
                            var event = new CustomEvent('payment.success', {
                                detail: response,
                                bubbles: true,
                                cancelable: true,
                            });
                            window.dispatchEvent(event);
                        };
                        try {
                            var rzp = new Razorpay(options);
                            rzp.open();
                            rzp.on('payment.failed', function (response) {
                                // Todo - store this information in the server
                                // console.log(response.error.code);
                                // console.log(response.error.description);
                                // console.log(response.error.source);
                                // console.log(response.error.step);
                                // console.log(response.error.reason);
                                // console.log(response.error.metadata.order_id);
                                // console.log(response.error.metadata.payment_id);
                                /* API: post /pg_order/phonepe/:order_type
                                order_type can be
                                1. wallet : for wallet recharge order
                                2. registration : for registration order
                                    payload :{
                                        "amount" : 100 // required only when order type is wallet
                                } */
                                var event = new CustomEvent('payment.failed', {
                                    detail: response.error.description,
                                    bubbles: true,
                                    cancelable: true,
                                });
                                window.dispatchEvent(event);
                            });
                        } catch (error) {
                            console.log(error);
                        }
                    } else {
                        this.loading = false;
                    }
                });
        }
    }

    @HostListener('window:payment.success', ['$event'])
    onPaymentSuccess(event): void {
        this.walletRechargeLoading = false;
        this.onRazorPaySuccess(event.detail);
    }

    @HostListener('window:payment.failed', ['$event'])
    onPaymentFailed(event): void {
        this.walletRechargeLoading = false;
        //console.log(JSON.stringify(event.detail));
        this.displayMessage('error','Error',event.detail)
    }

    razorPayResponse(response) {
        console.log(response);
        const self = this;
        // self.displayMessage('success','Success',JSON.stringify(response))
        this.onRazorPaySuccess(response);
    }

    onRazorPaySuccess(response) {
        var payload = {
            signature: response.razorpay_signature,
            order_id: response.razorpay_order_id,
            payment_ref: response.razorpay_payment_id,
        };
        this.apiService
            .postTypeRequest('mark_wallet_recharge', payload)
            .toPromise()
            .then((response: any) => {
                if (response.result) {
                    this.displayMessage('success', 'Success', response.message);
                }
            });
    }

    displayMessage(severity: any, summary: any, message: any) {
        this.messageService.add({
            severity: severity,
            summary: summary,
            detail: message,
        });
    }

    preBook(){
        const ref = this.dialogService.open(AddMembershipComponent,{
            data: {
                memberData: this.memberData,
            },
            header: `Pre Booking`,
            styleClass: 'w-10 sm:w-10 md:w-10 lg:w-6',
        })
        ref.onClose.subscribe((result: any) => {
            if (result) {
                this.loadData()
            }
        });
    }
}
