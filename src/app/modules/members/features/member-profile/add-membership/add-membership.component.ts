import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import {
    DialogService,
    DynamicDialogConfig,
    DynamicDialogRef,
} from 'primeng/dynamicdialog';
import { ApiService } from 'src/app/core/services/api/api.service';

@Component({
    selector: 'app-add-membership',
    templateUrl: './add-membership.component.html',
    styleUrls: ['./add-membership.component.scss'],
})
export class AddMembershipComponent implements OnInit {
    loading: boolean = false;
    memberDetails: any;
    accounts = [];
    minDate: Date;
    membershipTypeList: any[];
    selectedMembershipType: any[] = [];
    selectedMembershipAmount: any;
    payment_account_head_id_required: boolean = false;
    commonForm: FormGroup = new FormGroup({
        member_id: new FormControl('', [Validators.required]),
        meal_pack_id: new FormControl('', [Validators.required]),
        total_meal_packs: new FormControl(1, [Validators.required]),
        start_date: new FormControl(new Date().toISOString().substring(0, 10), [
            Validators.required,
        ]),
        paid_amount: new FormControl(),
        net_amount: new FormControl(),
        membership_amount: new FormControl(),
    });

    constructor(
        public config: DynamicDialogConfig,
        public ref: DynamicDialogRef,
        public apiService: ApiService,
        public dialogService: DialogService,
        public messageService: MessageService
    ) {}

    ngOnInit(): void {
        this.minDate = new Date();
        this.minDate.setDate(new Date().getDate()+1)
        this.commonForm.controls.member_id.setValue(this.config.data.memberData.member_id);
        this.commonForm.controls.start_date.setValue(
            new Date().toISOString().substring(0, 10)
        );
        // this.commonForm.controls.payment_date.setValue(
        //     new Date().toISOString().substring(0, 10)
        // );
        this.selectedMembershipAmount = '';
        this.memberDetails = this.config.data.memberData;
        // this.apiService
        //     .getTypeRequest(`table_data/INCOME_ACCOUNT_HEAD`)
        //     .toPromise()
        //     .then((result: any) => {
        //         this.loading = false;
        //         if (result.result) {
        //             this.accounts = result.data;
        //         }
        //     });
        this.getMealPacks();
    }

    addId(event: any) {
        if (event.value != null) {
            var data = this.membershipTypeList.filter((element) => {
                if (element.meal_pack_id == event.value) {
                    return element;
                } else {
                    return null;
                }
            });
            this.selectedMembershipAmount = 0;
            this.selectedMembershipType = data[0].meal_pack_items;
            this.selectedMembershipAmount = Number(data[0].meal_pack_price);
            if (this.selectedMembershipAmount > 0) {
                this.commonForm.controls.net_amount.setValue(
                    Number(this.selectedMembershipAmount) -
                        Number(this.memberDetails?.balance)
                );
            } else {
                this.commonForm.controls.net_amount.setValue(
                    Number(this.selectedMembershipAmount)
                );
            }
            this.commonForm.controls['total_meal_packs'].setValue(1)
            this.updateNetPayable();

        } else {
            this.selectedMembershipType = [];
        }
    }

    getMealPacks() {
        this.apiService
            .getTypeRequest(`meal_pack_data`)
            .toPromise()
            .then((resopnse: any) => {
                if (resopnse.result) {
                    this.membershipTypeList = resopnse.data;
                }
            });
    }

    updateNetPayable() {
        var total_meal_packs = Number(
            this.commonForm.controls['total_meal_packs'].value
        );
        if (total_meal_packs > 0) {
            this.commonForm.controls['net_amount'].setValue(
                total_meal_packs * this.selectedMembershipAmount -
                    Number(this.memberDetails?.balance)
            );
            this.commonForm.controls['membership_amount'].setValue(
                total_meal_packs * this.selectedMembershipAmount
            );
        } else {
            this.commonForm.controls['net_amount'].setValue(
                this.selectedMembershipAmount -
                    Number(this.memberDetails?.balance)
            );
            this.commonForm.controls['membership_amount'].setValue(
                this.selectedMembershipAmount
            );
        }
    }

    submitClick() {
        if(this.commonForm.controls['membership_amount'].value > this.memberDetails?.balance){
            this.messageService.add({
                severity: 'error',
                summary: 'Low Balance',
                detail: 'Please recharge and try again!!!',
            });
        }
        else if (this.commonForm.valid) {
            this.add();
        } else {
            var controls = this.commonForm.controls;
            for (const name in controls) {
                controls[name].markAsDirty();
                controls[name].markAllAsTouched();
            }
            this.messageService.add({
                severity: 'error',
                summary: 'Invalid',
                detail: 'Enter Required Details',
            });
        }
    }

    add() {
        this.loading = true;
        this.apiService
            .postTypeRequest(`register_membership/new`, this.commonForm.value)
            .toPromise()
            .then((resopnse: any) => {
                if (resopnse.result) {
                    this.ref.close(true);
                    this.messageService.add({
                        severity: 'success',
                        summary: 'Success',
                        detail: resopnse.message,
                    });
                    // this.dialogService.open(NewMembershipReceiptComponent, {
                    //     data: resopnse.data.membership,
                    //     header: `New MemberShip`,
                    //     styleClass: 'w-8  xs:w-12 sm:w-12 md:w-10 lg:w-5',
                    // });
                }
            })
            .finally(() => (this.loading = false));
    }

    updateRequiredFields() {
        if (this.commonForm.controls.paid_amount.value) {
            this.commonForm.controls.payment_account_head_id.addValidators(
                Validators.required
            );
            this.payment_account_head_id_required = true;
        } else {
            this.commonForm.controls.payment_account_head_id.clearValidators();
            this.payment_account_head_id_required = false;
        }
    }
}
