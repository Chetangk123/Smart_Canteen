import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-display-menu',
  templateUrl: './display-menu.component.html',
  styleUrls: ['./display-menu.component.scss']
})
export class DisplayMenuComponent implements OnInit {

  constructor(
        public config: DynamicDialogConfig) { }

  ngOnInit(): void {
    console.log(this.config.data);

  }

}
