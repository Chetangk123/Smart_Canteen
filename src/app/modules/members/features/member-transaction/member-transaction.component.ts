import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ApiService } from 'src/app/core/services/api/api.service';
import { AuthService } from '../../core/services/auth/auth.service';

@Component({
  selector: 'app-member-transaction',
  templateUrl: './member-transaction.component.html',
  styleUrls: ['./member-transaction.component.scss']
})
export class MemberTransactionComponent implements OnInit {

    loading: boolean = false;
    user:any
    commonForm: FormGroup = new FormGroup({
        customer_phnumber: new FormControl(),
        customer_email: new FormControl(),
        customer_name: new FormControl(),
        amount: new FormControl('', Validators.required),
    });
    constructor(
        private config: DynamicDialogConfig,
        private apiService: ApiService,
        private ref: DynamicDialogRef,
        private authService: AuthService,
        private messageService: MessageService
    ) {}

    ngOnInit() {
        this.commonForm.patchValue(this.config.data);
        this.user = this.authService.getUser()
        console.log(this.user);

    }

    submitClick() {
        if (this.commonForm.valid) {
            this.loading = true;
            this.ref.close(this.commonForm.controls['amount'].value)
        }
    }

    responseHandler(response:any){
        console.log(response);
        this.messageService.add({
            severity: 'success',
            summary: "summary",
            detail: "message",
        });
    }

    displayMessage(type:any, summary:any, message:any) {
        this.messageService.add({
            severity: type,
            summary: summary,
            detail: message,
        });
    }
}
