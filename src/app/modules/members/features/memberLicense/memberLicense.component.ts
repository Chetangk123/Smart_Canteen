import { Component, HostListener, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { ApiService } from 'src/app/core/services/api/api.service';
import { MemberService } from '../../core/services/MemberService/member.service';
import { AuthService } from '../../core/services/auth/auth.service';

declare var Razorpay: any;

@Component({
    selector: 'app-memberLicense',
    templateUrl: './memberLicense.component.html',
    styleUrls: ['./memberLicense.component.scss'],
})
export class MemberLicenseComponent implements OnInit {
    memberData: any;
    loading:boolean = false
    constructor(
        public memberService: MemberService,
        public apiService: ApiService,
        public authService: AuthService,
        public messageService: MessageService,
    ) {}

    ngOnInit() {
        this.memberData = this.memberService.getMemberData();
    }

    payNow() {
        var payload = {
            customer_phnumber: this.memberData.parents_ph,
            customer_email: this.memberData.email,
            customer_name: this.memberData.full_name,
            amount: 200,
        };
        this.apiService
            .postTypeRequest('registration_order', payload)
            .toPromise()
            .then((response: any) => {
                console.log(response);
                if (response.result) {
                    var options = response.data;
                    options.image = this.authService.getUser().dp_location;
                    const self = this;
                    options.handler = function (response) {
                        var event = new CustomEvent('payment.success', {
                            detail: response,
                            bubbles: true,
                            cancelable: true,
                        });
                        window.dispatchEvent(event);
                    };
                    try {
                        var rzp = new Razorpay(options);
                        rzp.open();
                        rzp.on('payment.failed', function (response) {
                            // Todo - store this information in the server
                            // console.log(response.error.code);
                            // console.log(response.error.description);
                            // console.log(response.error.source);
                            // console.log(response.error.step);
                            // console.log(response.error.reason);
                            // console.log(response.error.metadata.order_id);
                            // console.log(response.error.metadata.payment_id);
                            var event = new CustomEvent('payment.failed', {
                                detail: response.error.description,
                                bubbles: true,
                                cancelable: true,
                            });
                            window.dispatchEvent(event);
                        });
                    } catch (error) {
                        console.log(error);
                    }
                } else {
                    this.loading = false;
                }
            });
    }

    @HostListener('window:payment.success', ['$event'])
    onPaymentSuccess(event): void {
        this.onRazorPaySuccess(event.detail);
    }

    @HostListener('window:payment.failed', ['$event'])
    onPaymentFailed(event): void {
        //console.log(JSON.stringify(event.detail));
        this.displayMessage('error','Error',event.detail)
    }

    razorPayResponse(response) {
        console.log(response);
        const self = this;
        // self.displayMessage('success','Success',JSON.stringify(response))
        this.onRazorPaySuccess(response);
    }

    onRazorPaySuccess(response) {
        var payload = {
            signature: response.razorpay_signature,
            order_id: response.razorpay_order_id,
            payment_ref: response.razorpay_payment_id,
        };
        this.apiService
            .postTypeRequest('save_registration_payment', payload)
            .toPromise()
            .then((response: any) => {
                if (response.result) {
                    this.displayMessage('success', 'Success', response.message);
                    this.authService.logout()
                }
            });
    }

    displayMessage(severity: any, summary: any, message: any) {
        this.messageService.add({
            severity: severity,
            summary: summary,
            detail: message,
        });
    }
}
