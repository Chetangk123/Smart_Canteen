import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MembersRoutingModule } from './members-routing.module';
import { MemberProfileComponent } from './features/member-profile/member-profile.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrimengModule } from 'src/app/shared/primeng/primeng.module';
import { MemberTransactionsComponent } from './features/reports/member-transactions/member-transactions.component';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { LoginComponent } from './pages/login/login.component';
import { AppConfigComponent } from './layout/app.config.component';
import { AppFooterComponent } from './layout/app.footer.component';
import { AppMainComponent } from './layout/app.main.component';
import { AppMenuComponent } from './layout/app.menu.component';
import { AppMenuitemComponent } from './layout/app.menuitem.component';
import { AppTopBarComponent } from './layout/app.topbar.component';
import { MemberTransactionComponent } from './features/member-transaction/member-transaction.component';
import { MemberCardHistoryComponent } from './features/reports/member-card-history/member-card-history.component';
import { MemberLeaveHistoryComponent } from './features/reports/member-leave-history/member-leave-history.component';
import { MemberMembershipHistoryComponent } from './features/reports/member-membership-history/member-membership-history.component';
import { MemberLicenseComponent } from './features/memberLicense/memberLicense.component';
import { AddMembershipComponent } from './features/member-profile/add-membership/add-membership.component';
import { DisplayMenuComponent } from './features/member-profile/display-menu/display-menu.component';

@NgModule({
    declarations: [
        LoginComponent,
        AppMainComponent,
        AppTopBarComponent,
        AppFooterComponent,
        AppConfigComponent,
        AppMenuComponent,
        AppMenuitemComponent,
        MemberProfileComponent,
        MemberTransactionsComponent,
        MemberTransactionComponent,
        MemberCardHistoryComponent,
        MemberLeaveHistoryComponent,
        MemberMembershipHistoryComponent,
        MemberLicenseComponent,
        AddMembershipComponent,
        DisplayMenuComponent
    ],
    imports: [
        CommonModule,
        PrimengModule,
        FormsModule,
        ReactiveFormsModule,
    MembersRoutingModule
    ],
    providers: [DynamicDialogRef, DynamicDialogConfig],
})
export class MembersModule {}
