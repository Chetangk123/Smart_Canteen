import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { MemberProfileComponent } from './features/member-profile/member-profile.component';
import { AppMainComponent } from './layout/app.main.component';
import { MemberGuard } from './core/guard/member.guard';
import { MemberLicenseComponent } from './features/memberLicense/memberLicense.component';
import { LicenseGuard } from './core/guard/license.guard';

const routes: Routes = [
    {
        path:'login',
        component: LoginComponent
    },
    {
        path:'',
        component: AppMainComponent,
        children:[
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'profile',
            },
            {
                path:'profile',
                canActivate:[MemberGuard],
                component:MemberProfileComponent
            }
        ]
    },
    {
        path:'memberLicense',
        canActivate:[LicenseGuard],
        component:MemberLicenseComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MembersRoutingModule { }
