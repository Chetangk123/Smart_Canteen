import { Component, OnDestroy, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { ApiService } from 'src/app/core/services/api/api.service';
import { MealPackService } from '../../mealPack.service';
import { Subject, forkJoin, map, takeUntil } from 'rxjs';
import {
    FormBuilder,
    FormControl,
    FormGroup,
    Validators,
} from '@angular/forms';

export type MealPacks = {
    mealPacksArray?: MealPack[];
};

export type MealPack = {
    id:number;
    class_id?: number;
    class_name?: number;
    division_id?: number;
    division_name?: number;
    meal_items?: MealPackItem[];
};

export type MealPackItem = {
    meal_id?: number;
    meal_name?: string;
    meal_start_time?: string;
    meal_end_time?: string;
    meal_price?: number;
};

@Component({
    selector: 'app-add-academic-constraints',
    templateUrl: './add-academic-constraints.component.html',
    styleUrls: ['./add-academic-constraints.component.scss'],
})
export class AddAcademicConstraintsComponent implements OnInit, OnDestroy {
    classList;
    divisionList;
    mealPackItems;
    commonForm: FormGroup;
    mealPacks: MealPacks = {
        mealPacksArray: [],
    };
    mealPack: MealPack = {
        id:null,
        class_id: null,
        division_id: null,
        meal_items: [],
    };
    accordionOpen: boolean = false;

    // Private
    private _unsubscribeAll: Subject<any> = new Subject<any>();
    constructor(
        public ref: DynamicDialogRef,
        public apiService: ApiService,
        public config: DynamicDialogConfig,
        public messageService: MessageService,
        private mealPackService: MealPackService,
        private formBuilder: FormBuilder
    ) {}

    ngOnInit(): void {
        this.commonForm = this.formBuilder.group({
            class_id: ['', Validators.required],
            division_id: [''],
        });
        this.classList = this.config.data.classList;
        this.divisionList = this.config.data.divisionList;
        this.mealPack = {
            id:null,
            class_id: null,
            division_id: null,
            meal_items: [],
        };
        this.config.data.mealPack.meal_pack_items.forEach(
            (item: any) => {
                let ArrayItem: MealPackItem = {
                    meal_id: Number(item.meal_id),
                    meal_name: item.meal_name,
                    meal_start_time: item.meal_start_time,
                    meal_end_time: item.meal_end_time,
                    meal_price: Number(item.meal_price),
                };
                this.mealPack.meal_items.push(ArrayItem);
            }
        );
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }

    getCompositeKey(item: any): string {
        debugger
        return `${item.id1}_${item.id2}`;
    }

    hasNullValues(obj: any): boolean {
        for (const key in obj) {
            if (key != 'division_id' && obj.hasOwnProperty(key)) {
                const value = obj[key];
                if (value === null || value === '') {
                    return true;
                }
                if (typeof value === 'object') {
                    if (this.hasNullValues(value)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    addElementToList() {
        try {
            if (this.commonForm.valid) {
                this.mealPack.id = this.mealPacks.mealPacksArray.length+1,
                this.mealPack.class_id =
                    this.commonForm.controls['class_id'].value;
                    this.mealPack.class_name = this.classList.filter(item =>item.id === this.commonForm.controls['class_id'].value).map(filteredItem => filteredItem.name)[0];
                this.mealPack.division_id =
                    this.commonForm.controls['division_id'].value;
                    if(this.commonForm.controls['division_id'].value){
                        this.mealPack.division_name = this.divisionList.filter(item =>item.id === this.commonForm.controls['division_id'].value).map(filteredItem => filteredItem.name)[0];
                    }
                if (!this.hasNullValues(this.mealPack)) {

                    this.mealPacks.mealPacksArray.push(
                        this.mealPack
                    );
                    this.ngOnInit();
                    this.accordionOpen = false
                } else {
                    throw new Error('All fields Required')
                }
            } else {
                this.commonForm.markAllAsTouched()
                throw new Error('All fields Required')
            }
        } catch (error) {
            this.messageService.add({
                severity: 'error',
                summary: 'Error',
                detail: `${error.message}`,
            });
        }
    }

    submit() {
        const payload = {
            meal_pack_id: this.config.data.mealPack.meal_pack_id,
            constraints_array: this.mealPacks.mealPacksArray,
        };
        this.apiService
            .postTypeRequest('mp_academic_constraint_ops/insert', payload)
            .toPromise()
            .then((response: any) => {
                console.log(response);
                if (response.result) {
                    this.messageService.add({
                        severity: 'success',
                        summary: 'Success',
                        detail: `${response.message}`,
                    });
                    this.ref.close(true)
                }
            });
    }
}
