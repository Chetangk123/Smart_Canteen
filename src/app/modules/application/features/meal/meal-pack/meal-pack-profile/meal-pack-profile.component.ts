import { Component, OnDestroy, OnInit } from '@angular/core';
import { MealPackService } from '../mealPack.service';
import { ApiService } from 'src/app/core/services/api/api.service';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { CounterService } from '../../../counters/counter.service';
import { MealPack } from 'src/app/core/interfaces/mealPack';
import moment from 'moment';
import { ConfigureMealPackComponent } from '../configure-meal-pack/configure-meal-pack.component';
import { Observable, Subject, map, takeUntil } from 'rxjs';
import { AddAcademicConstraintsComponent } from './add-academic-constraints/add-academic-constraints.component';

@Component({
    selector: 'app-meal-pack-profile',
    templateUrl: './meal-pack-profile.component.html',
    styleUrls: ['./meal-pack-profile.component.scss'],
})
export class MealPackProfileComponent implements OnInit, OnDestroy {
    public mealPack: MealPack | null = null;
    public Data
    public meals
    public classList
    public divisionList
    public selectedProduct
    public academicConstraints: Observable<Object>;
    public academicConstraintsLoading:boolean = false
    public loading: boolean = false

    // Private
    private _unsubscribeAll: Subject<any> = new Subject<any>();
    constructor(
        private apiService: ApiService,
        private mealPackService: MealPackService,
        private confirmationService: ConfirmationService,
        private messageService: MessageService,
        private dialogService: DialogService,
        private counterService: CounterService,
    ) {}

    ngOnInit(): void {
        this.loadData()
        this.loadAcademicConstraints()
        this.apiService
            .getTypeRequest(`table_data/CLASS`)
            .pipe(
                takeUntil(this._unsubscribeAll),
                map((response:any)=>{
                    if(response.result){
                        this.classList = response.data
                    }
                })
            ).subscribe()
            this.apiService
            .getTypeRequest(`table_data/DIVISION`)
            .pipe(
                takeUntil(this._unsubscribeAll),
                map((response:any)=>{
                    if(response.result){
                        this.divisionList = response.data
                    }
                })
            ).subscribe()
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }

    loadData(){
        this.loading = true
        this.mealPackService.mealPack$.pipe(takeUntil(this._unsubscribeAll))
        .subscribe((data: any) => {
            this.loading = false
            this.mealPack = data;
            this.meals = this.mealPack.meal_pack_items
            this.meals.map((item: any) => {
                item.meal_start_time = moment(
                    item.meal_start_time,
                    'hh:mm:ss A'
                ).format('HH:mm:ss');
                item.meal_end_time = moment(
                    item.meal_end_time,
                    'hh:mm:ss A'
                ).format('HH:mm:ss');
            });
            this.mealPack.meal_pack_items = this.meals
        });
    }

    loadMeals(){

    }

    loadAcademicConstraints(){
        this.academicConstraintsLoading = true;
        this.academicConstraints = this.apiService.getTypeRequest(`table_data/MP_ACADEMIC_CONSTRAINTS_BY_MEAL_PACK_ID/${this.mealPack.meal_pack_id}`)
        .pipe( map((response:any)=>{
            this.academicConstraintsLoading = false
            if(response.result){
                return response.data
            } else {
                return null;
            }
        }))
    }

    addMeals(){
        const ref = this.dialogService.open(ConfigureMealPackComponent, {
            header: `Configure Meal Pack - ${this.mealPack.meal_pack_name}`,
            styleClass: 'w-10 sm:w-10 md:w-10 lg:w-8',
            data: this.mealPack,
        });
        ref.onClose.subscribe((result: any) => {
            if (result) {
                this.mealPackService.getUpdatedMealPack(this.mealPack.meal_pack_id)
                this.ngOnInit();
            }
        });
    }

    updateMeal(product: any) {
        this.apiService
            .postTypeRequest('meal_pack_ops/update', this.getPayload(product))
            .toPromise()
            .then((result: any) => {
                if (result.result) {
                    this.mealPackService.getUpdatedMealPack(this.mealPack.meal_pack_id)
                    //this.ngOnInit();
                }
            });
    }

    confirm(product: any) {
        this.confirmationService.confirm({
            message: 'Do you want to delete this record?',
            header: 'Delete Confirmation',
            icon: 'pi pi-info-circle',
            accept: () => {
                this.deleteMeal(product);
            },
        });
    }

    deleteMeal(product: any) {
        this.apiService
            .postTypeRequest('meal_pack_ops/delete', this.getPayload(product))
            .toPromise()
            .then((result: any) => {
                if (result.result) {
                    this.mealPackService.getUpdatedMealPack(this.mealPack.meal_pack_id)
                    //this.ngOnInit()
                }
            });
    }

    getPayload(product: any) {
        return {
            meal_pack_item_id: product.meal_pack_item_id,
            meal_price: product.meal_price,
            meal_start_time: moment(
                product.meal_start_time,
                'hh:mm:ss A'
            ).format('hh:mm:ss A'),
            meal_end_time: moment(product.meal_end_time, 'hh:mm:ss A').format(
                'hh:mm:ss A'
            ),
        };

    }

    AddAcademicConstraintsComponent(){
        const ref = this.dialogService.open(AddAcademicConstraintsComponent,{
            header:'Add Academic Constraints',
            styleClass: 'w-10 sm:w-10 md:w-10 lg:w-7',
            data:{
                mealPack:this.mealPack,
                classList:this.classList,
                divisionList:this.divisionList
            }
        })
        ref.onClose.subscribe((result: any) => {
            if (result) {
                this.loadAcademicConstraints();
            }
        });
    }

    updateAcademicConstraint(product:any){
        var payload = {
            "mp_academic_constraint_id": product.mp_academic_constraint_id
        }
        this.apiService.postFileTypeRequest('mp_academic_constraint_ops/delete',payload)
        .pipe(
            map((response:any)=>{
                if(response.result){
                    this.loadAcademicConstraints();
                }
            })
        )
    }

    confirmAcademicConstraint(product: any) {
        this.confirmationService.confirm({
            message: 'Do you want to delete this record?',
            header: 'Delete Confirmation',
            icon: 'pi pi-info-circle',
            accept: () => {
                this.deleteAcademicConstraint(product);
            },
        });
    }

    deleteAcademicConstraint(product: any) {
        var payload = {
            "meal_pack_item_id": product.meal_pack_item_id
        }
        this.apiService.postFileTypeRequest('mp_academic_constraint_ops/delete',payload)
        .pipe(
            takeUntil(this._unsubscribeAll),
            map((response:any)=>{
                if(response.result){
                    this.loadAcademicConstraints();
                }
            })
        ).subscribe()
    }
}
