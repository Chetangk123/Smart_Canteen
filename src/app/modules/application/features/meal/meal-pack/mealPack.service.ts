import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

import * as CryptoJS from 'crypto-js';
import { MealPack } from 'src/app/core/interfaces/mealPack';
import { ApiService } from 'src/app/core/services/api/api.service';

@Injectable({
    providedIn: 'root',
})
export class MealPackService {
    private mealPack: MealPack | null = null;
    private readonly encPassword = environment.encPassword;

    private readonly mealPackSubject: BehaviorSubject<MealPack | null> =
        new BehaviorSubject<MealPack | null>(null);
    mealPack$: Observable<MealPack | null> =
        this.mealPackSubject.asObservable();

    private readonly mealsSubject: BehaviorSubject<MealPack | null> =
        new BehaviorSubject<MealPack | null>(null);
    meals$: Observable<MealPack | null> =
        this.mealsSubject.asObservable();

    constructor(
        private apiService: ApiService
    ) {
        this.loadMealPack();
    }

    setMealPack(newValue: MealPack) {
        this.mealPackSubject.next(newValue);
        this.mealPack = newValue;
        this.storeEncryptedData('mealPack', this.mealPack);
    }

    deleteMealPack() {
        localStorage.removeItem('mealPack');
        this.mealPack = null;
    }

    async getUpdatedMealPack(id:any){
        await this.apiService
            .getTypeRequest(
                `meal_pack_data?meal_pack_id=${id}`
            )
            .toPromise()
            .then((result: any) => {
                if (result.result) {
                    this.setMealPack(result.data)
                }
            })
    }

    getMealPack() {
        if (this.mealPack) {
            return this.mealPack;
        }
        const storedData = this.getDecryptedData('mealPack');
        if (storedData) {
            this.mealPack = storedData;
            this.mealPackSubject.next(this.mealPack);
            return this.mealPack;
        }
        return null;
    }

    private loadMealPack() {
        const storedData = this.getDecryptedData('mealPack');
        if (storedData) {
            this.mealPack = storedData;
            this.mealPackSubject.next(this.mealPack);
        }
    }

    private storeEncryptedData(key: string, data: any) {
        try {
            const encryptedData = CryptoJS.AES.encrypt(
                JSON.stringify(data),
                this.encPassword
            ).toString();
            localStorage.setItem(key, encryptedData);
        } catch (error) {
            // Handle encryption error
        }
    }

    private getDecryptedData(key: string) {
        try {
            const storedData = localStorage.getItem(key);
            if (storedData) {
                const decryptedData = CryptoJS.AES.decrypt(
                    storedData,
                    this.encPassword.trim()
                ).toString(CryptoJS.enc.Utf8);
                return JSON.parse(decryptedData);
            }
        } catch (error) {
            // Handle decryption error
        }

        return null;
    }
}
