import { Component, OnInit } from '@angular/core';
import { AppConfig } from 'src/app/core/interfaces/appconfig';
import { CoreConfig } from 'src/app/core/interfaces/coreConfig';
import { ApiService } from 'src/app/core/services/api/api.service';
import { ConfigService } from 'src/app/core/services/app.config.service';
import { EnvService } from 'src/app/env.service';
import { SettingsService } from '../../../../core/services/settings/settings.service';
import { MemberService } from '../../core/services/MemberService/member.service';
import { DialogService } from 'primeng/dynamicdialog';
import { UpdateImageComponent } from '../pos/update-image/update-image.component';

export interface Setting {
    settings_id: string;
    counter_id: string;
    isCounter: string;
    display_label: string;
    settings_name: string;
    settings_value: string | boolean | number;
    isDisplay: string;
}

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {
    card: string = 'Account Activity';
    public coreConfig: CoreConfig;
    loading: boolean = false;
    tableData: Setting[] = [];
    memberData: any;
    base_logo_url: any;
    menu_url: any;
    editIndex: any;
    config: AppConfig;
    editTheme: boolean = false;
    isImage: boolean = false;
    editDateRange: boolean = false;
    themeColor: Setting;
    dateRange: Setting;
    smsOnTap: Setting;
    minCardBalance: Setting;
    walletTransactionType: Setting;
    leaveEncashment: Setting;
    scanSuccessSound: Setting;
    scanFailureSound: Setting;
    LEAVE_ENCASHMENT_TYPE:Setting;
    cardNumberLength: Setting;
    constructor(
        public apiService: ApiService,
        public configService: ConfigService,
        public memberService: MemberService,
        private settingsService: SettingsService,
        public _coreEnvService: EnvService,
        public dialogService: DialogService
    ) {
        this.coreConfig = _coreEnvService.config;
    }

    ngOnInit(): void {
        this.settingsService.settingsDate$.subscribe(() => {
            //this.getUpdatedSettings()
        });
        this.editIndex = -1;
        this.memberData = this.memberService.getUserData();
        this.getUpdatedSettings();
        this.getOldSettings();
    }

    findSettingObject(targetSettingName) {
        const settingObj = this.tableData.find(
            (setting) => setting.settings_name === targetSettingName
        );
        return settingObj ? settingObj : null;
    }

    setCard(cardName) {
        this.card = cardName;
    }

    ChangePassword() {}

    updateSettings(data: any) {
        if (data.settings_name == 'THEME_COLOR') {
            data.settings_value =
                data.settings_value == 'light' ? 'dark' : 'light';
            this.updateThemeCheckedValue(data);
        } else if (
            data.settings_value == true ||
            data.settings_value == false
        ) {
            data.settings_value = data.settings_value ? 1 : 0;
        }
        this.apiService
            .postTypeRequest(`settings_ops/update`, data)
            .toPromise()
            .then((result: any) => {
                this.loading = false;
                if (result.result) {
                    this.editDateRange = false;
                    this.editIndex = -1;
                    this.getUpdatedSettings();
                }
            });
    }

    getThemeCheckedValue(data: any) {
        if (data == 'dark') {
            return true;
        } else {
            return false;
        }
    }

    updateThemeCheckedValue(data: any) {
        let themeElement = document.getElementById('theme-css');
        let dark: boolean;
        let theme: string;
        if (data.settings_value == 'dark') {
            dark = true;
            theme = 'lara-dark-indigo';
        } else {
            dark = false;
            theme = 'lara-light-indigo';
        }
        themeElement.setAttribute(
            'href',
            'assets/theme/' + theme + '/theme.css'
        );
        this.configService.updateConfig({ ...this.config, ...{ theme, dark } });
        this.getUpdatedSettings();
    }

    updateThemeValue() {
        this.editTheme = false;
        this.updateSettings(this.tableData[0]);
    }

    getUpdatedSettings() {
        var url;
        this.config = this.configService.config;
        if (this.memberData.user_role == 'OWNER') {
            url = 'table_data/CANTEEN_SETTINGS';
        } else {
            url = 'table_data/SETTINGS';
        }
        this.apiService
            .getTypeRequest(`${url}`)
            .toPromise()
            .then((result: any) => {
                this.loading = false;
                if (result.result) {
                    this.settingsService.updateSettingsDate(result.data);
                    this.tableData = result?.data;
                    this.themeColor = this.findSettingObject('THEME_COLOR');
                    this.dateRange = this.findSettingObject('DATE_RANGE');
                    this.LEAVE_ENCASHMENT_TYPE = this.findSettingObject('LEAVE_ENCASHMENT_TYPE');
                    this.smsOnTap = this.findSettingObject('SMS_ON_EVERY_TAP');
                    this.minCardBalance = this.findSettingObject(
                        'MINIMUM_CARD_BALANCE'
                    );
                    this.walletTransactionType = this.findSettingObject(
                        'WALLET_TRANSACTION_TYPE'
                    );
                    this.leaveEncashment = this.findSettingObject(
                        'LEAVE_ENCASHMENT_DAYS'
                    );
                    this.scanSuccessSound =
                        this.findSettingObject('SCAN_SUCCESS_SOUND');
                    this.scanFailureSound =
                        this.findSettingObject('SCAN_FAILURE_SOUND');
                    this.cardNumberLength =
                        this.findSettingObject('CARD_NUMBER_LENGTH');
                    console.log(this.minCardBalance);

                    // Update the properties with true/false values based on the transformation
                    this.smsOnTap.settings_value = this.getTrueFalse(
                        this.smsOnTap.settings_value
                    );
                    this.LEAVE_ENCASHMENT_TYPE.settings_value =
                        this.getTrueFalse(
                            this.LEAVE_ENCASHMENT_TYPE.settings_value
                        );
                    this.walletTransactionType.settings_value =
                        this.getTrueFalse(
                            this.walletTransactionType.settings_value
                        );
                    this.scanSuccessSound.settings_value = this.getTrueFalse(
                        this.scanSuccessSound.settings_value
                    );
                    this.scanFailureSound.settings_value = this.getTrueFalse(
                        this.scanFailureSound.settings_value
                    );
                } else {
                    this.tableData = [];
                }
            });
    }

    getTrueFalse(settings_value: any) {
        return settings_value == 1 ? true : false;
    }

    getOldSettings() {
        // table_data/SPECIFIC_SETTINGS
        this.loading = true;
        this.apiService
            .getTypeRequest(`specific_data/COUNTER/MY_COUNTER`)
            .toPromise()
            .then((result: any) => {
                this.loading = false;
                if (result.result) {
                    this.base_logo_url = result.data.logo_url;
                    this.menu_url = result.data.menu_url;
                    const fileExtension = this.getFileExtension(this.menu_url);
                    if (this.isImageExtension(fileExtension)) {
                        this.isImage = true; // 'Image';
                    } else if (this.isPdfExtension(fileExtension)) {
                        this.isImage = false; // 'PDF';
                    } else {
                        this.isImage = false;
                    }

                    console.log(this.isImage);
                }
            });
    }

    private getFileExtension(filename: string): string {
        return filename.split('.').pop()?.toLowerCase() || '';
    }

    private isImageExtension(extension: string): boolean {
        const imageExtensions = ['jpg', 'jpeg', 'png', 'gif'];
        return imageExtensions.includes(extension);
    }

    private isPdfExtension(extension: string): boolean {
        return extension === 'pdf';
    }

    updateImage() {
        const ref = this.dialogService.open(UpdateImageComponent, {
            header: `Update new menu`,
            data: {
                Url: 'file_upload/MENU_IMAGE'
            },
            width: '60%',
        });

        ref.onClose.subscribe((result: any) => {
            if (result) {
                this.getOldSettings();
            }
        });
    }

    async updateLogo(event: any, Url: string) {
        // this.logoLoading = true;
        const formData: FormData = new FormData();
        formData.append('file', event.target.files[0]);
        formData.append('token', this.apiService.getTocken());
        formData.append('item_id', this.memberData?.counter_id);

        await this.apiService
            .postFileTypeRequest(`file_upload/${Url}`, formData)
            .toPromise()
            .then((result: any) => {
                if (result.result) {
                    // this.messageService.add({
                    //     severity: 'success',
                    //     summary: 'Success',
                    //     detail: result.message,
                    // });
                    this.getUpdatedSettings();
                    this.getOldSettings();
                }
            });
    }
}
