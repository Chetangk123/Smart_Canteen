export interface filteredMembershipData {
    counter_id?:     string;
    meal_pack_id?:   string;
    meal_pack_name?: string;
    no_of_members?:  string;
    member_data?:    MemberData[];
}

export interface MemberData {
    member_id?:      string;
    card_number?:    string;
    counter_id?:     string;
    full_name?:      string;
    gender?:         string;
    phone_number?:   string;
    parents_ph?:     string;
    dob?:            string;
    email?:          null;
    school_name?:    string;
    class_name?:     string;
    class_id?:       null;
    division_name?:  null;
    division_id?:    null;
    hostel_details?: null;
    photo_url?:      string;
    profile_photo?:  string;
    member_type_id?: string;
    member_type?:    string;
    address?:        string;
    status?:         string;
    balance?:        string;
    license_status?: string;
}
