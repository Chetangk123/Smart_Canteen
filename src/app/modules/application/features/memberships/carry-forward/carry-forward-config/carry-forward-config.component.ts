import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Message, MessageService } from 'primeng/api';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { Subject, map, takeUntil } from 'rxjs';
import { ApiService } from 'src/app/core/services/api/api.service';
import { MemberData } from '../filteredMembershipData';
import { CoreConfig } from 'src/app/core/interfaces/coreConfig';
import { SettingsService } from 'src/app/core/services/settings/settings.service';
import moment from 'moment';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-carry-forward-config',
    templateUrl: './carry-forward-config.component.html',
    styleUrls: ['./carry-forward-config.component.scss'],
})
export class CarryForwardConfigComponent implements OnInit, OnDestroy {
    minDate: Date;
    cardNumber: any;
    messages: Message[] = [];
    selectedMember: any;
    accordionOpen: boolean = false;
    loading: boolean = false;
    submitLoading: boolean = false;
    membershipTypeList: any[];
    membersList: any[];
    filteredMembers: any[];
    selectedMembershipType: any[] = [];
    accountList: any[] = [];
    selectedMembershipAmount: any;
    membersToAddList: MemberData[];
    commonForm: FormGroup = new FormGroup({
        member_ids: new FormControl(null),
        meal_pack_id: new FormControl('', [Validators.required]),
        total_meal_packs: new FormControl(1, [Validators.required]),
        max_days: new FormControl(1, [Validators.required]),
        start_date: new FormControl(new Date(), [Validators.required]),
        payment_date: new FormControl(
            new Date().toISOString().substring(0, 10),
            [Validators.required]
        ),
        payment_mode: new FormControl(''),
        payment_ref: new FormControl(''),
        payment_comments: new FormControl(''),
        paid_amount: new FormControl(),
        net_amount: new FormControl(),
        membership_amount: new FormControl(),
        payment_account_head_id: new FormControl(),
    });

    // Private
    private _unsubscribeAll: Subject<any> = new Subject<any>();
    constructor(
        public ref: DynamicDialogRef,
        public apiService: ApiService,
        public config: DynamicDialogConfig,
        public messageService: MessageService,
        public settingService: SettingsService,
        private datePipe: DatePipe
    ) {}

    ngOnInit(): void {
        this.minDate = new Date();
        this.minDate.setDate(new Date().getDate());
        this.membersToAddList = this.config.data.member_data;
        this.getMemberships();
        this.getAccounts();
        this.getMembers();
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }

    getMemberships() {
        this.apiService
            .getTypeRequest('meal_pack_data')
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((response: any) => {
                if (response.result) {
                    this.membershipTypeList = response.data;
                    this.commonForm.controls.payment_date.setValue(
                        new Date().toISOString().substring(0, 10)
                    );
                    this.commonForm.controls.meal_pack_id.setValue(
                        this.config.data.meal_pack_id
                    );
                    const data = {
                        value: this.config.data.meal_pack_id,
                    };
                    this.addId(data);
                }
            });
    }

    getMembers() {
        this.apiService
            .getTypeRequest(`table_data/MEMBER`)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((result: any) => {
                if (result.result) {
                    this.membersList = result?.data;
                } else {
                    this.membersList = [];
                }
            });
    }

    getAccounts() {
        this.apiService
            .getTypeRequest(`table_data/INCOME_ACCOUNT_HEAD`)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((result: any) => {
                this.loading = false;
                if (result.result) {
                    this.accountList = result.data;
                }
            });
    }

    addId(event: any) {
        if (event.value != null) {
            var data = this.membershipTypeList.filter((element) => {
                if (element.meal_pack_id == event.value) {
                    return element;
                } else {
                    return null;
                }
            });
            this.selectedMembershipAmount = 0;
            this.selectedMembershipType = data[0].meal_pack_items;
            this.selectedMembershipAmount = Number(data[0].meal_pack_price);

            this.commonForm.controls.net_amount.setValue(
                Number(this.selectedMembershipAmount)
            );
            this.commonForm.controls['total_meal_packs'].setValue(1);
            //this.updateNetPayable();
        } else {
            this.selectedMembershipType = [];
        }
    }

    filterMembers(event: any) {
        let filtered: any[] = [];
        let query = event.query;

        for (let i = 0; i < this.membersList.length; i++) {
            let member = this.membersList[i];
            if (
                member.full_name.toLowerCase().includes(query.toLowerCase()) ||
                member.card_number.includes(query)
            ) {
                filtered.push(member);
            }
        }
        //74475537390125
        this.filteredMembers = filtered;
    }

    saveMembershipData(): boolean {
        console.log(this.commonForm.value);
        if (this.commonForm.valid) {
            this.accordionOpen = false;
            return true;
        } else {
            var controls = this.commonForm.controls;
            for (const name in controls) {
                controls[name].markAsDirty();
                controls[name].markAllAsTouched();
            }
            this.messageService.add({
                severity: 'error',
                summary: 'Invalid',
                detail: 'Enter Required Details',
            });
            return false;
        }
    }

    fetchMemberData() {
        if (this.cardNumber.length == 14) {
            const cardNumber = this.cardNumber;
            this.cardNumber = '';
            this.loading = true;
            this.apiService
                .getTypeRequest(`specific_data/MEMBER_DATA/${cardNumber}`)
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe((resopnse: any) => {
                    if (resopnse.result) {
                        console.log(resopnse.data);
                        this.membersToAddList.push(resopnse.data);
                        // this.commonForm.controls.net_amount.setValue(this.memberDetails.balance);
                        this.messageService.add({
                            severity: 'success',
                            summary: resopnse.message,
                            detail: 'Found Card Details.',
                        });
                    } else {
                        this.messageService.add({
                            severity: 'error',
                            summary: resopnse.message,
                            detail: 'Card Details Not Found.',
                        });
                    }
                    this.loading = false;
                });
        }
    }

    addMemberData(data: any) {
        console.log(data);
        this.membersToAddList.push(data);
    }

    submit() {
        if (this.saveMembershipData()) {
            var membersIdList = [];
            var start_date = this.datePipe.transform(
                this.commonForm.controls.start_date.value,
                'yyyy-MM-dd'
            );
            this.commonForm.controls.start_date.setValue(start_date);
            this.membersToAddList.forEach((member: any) => {
                membersIdList.push(Number(member.member_id));
            });
            this.commonForm.controls.member_ids.setValue(membersIdList);
            this.apiService
                .postTypeRequest(
                    'membership_registration',
                    this.commonForm.value
                )
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe((resopnse: any) => {
                    this.loading = false;
                    if (resopnse.result) {
                        this.commonForm.reset();
                        this.membersToAddList = [];
                        this.messages =[];
                        this.ref.close(true);
                    } else {
                        // "data": {
                        //     "success_data": [],
                        //     "failed_data": [
                        //         {
                        //             "member_id": 1,
                        //             "membership_res": {
                        //                 "result": false,
                        //                 "message": "Active membership found, cannot register a new membership!",
                        //                 "status": "danger",
                        //                 "icon": "error"
                        //             }
                        //         },
                        //         {
                        //             "member_id": 5,
                        //             "membership_res": {
                        //                 "result": false,
                        //                 "message": "Active membership found, cannot register a new membership!",
                        //                 "status": "danger",
                        //                 "icon": "error"
                        //             }
                        //         }
                        //     ]
                        // }
                        this.messageService.add({
                            severity: 'error',
                            summary: resopnse.message,
                            detail: 'Error.',
                        });
                        var errorMessageList: any[] = [];
                        resopnse.data.failed_data.forEach((errorMessage) => {
                            var errorMessage: any = {
                                severity: 'error',
                                summary: 'Error',
                                detail: errorMessage.membership_res.message,
                            };
                            errorMessageList.push(errorMessage);
                        });
                        this.messages = errorMessageList;
                    }
                });
        }
    }
}
