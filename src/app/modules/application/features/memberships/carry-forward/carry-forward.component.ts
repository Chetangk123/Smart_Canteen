import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { ApiService } from 'src/app/core/services/api/api.service';
import { MemberService } from '../../../core/services/MemberService/member.service';
import { DatePipe } from '@angular/common';
import { Observable, Subject, map } from 'rxjs';
import { filteredMembershipData } from './filteredMembershipData';
import { CarryForwardConfigComponent } from './carry-forward-config/carry-forward-config.component';

@Component({
    selector: 'app-carry-forward',
    templateUrl: './carry-forward.component.html',
    styleUrls: ['./carry-forward.component.scss'],
})
export class CarryForwardComponent implements OnInit, OnDestroy {
    start_date: any;
    end_date: any;
    datePipe: DatePipe = new DatePipe('en-US');
    filteredMembershipData: Observable<filteredMembershipData[]>;
    filteredMembershipDataLoading: boolean = false;
    // Private
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    constructor(
        public apiService: ApiService,
        private confirmationService: ConfirmationService,
        public messageService: MessageService,
        public dialogService: DialogService,
        public route: ActivatedRoute,
        public memberService: MemberService,
        public router: Router
    ) {}

    ngOnInit(): void {
        var userData = this.memberService.getUserData();
        var transaction_range = userData?.settings[1]?.settings_value ?? 30;
        this.end_date = new Date().toISOString().substring(0, 10);
        this.start_date = this.datePipe.transform(
            new Date().setDate(new Date().getDate() - transaction_range),
            'yyyy-MM-dd'
        );
        this.getFilteredMembershipData()
    }
    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }

    getFilteredMembershipData() {
        this.filteredMembershipDataLoading = true;
        var dateFilter = ``;
        if (this.start_date != null && this.end_date != null) {
            const start_date = this.datePipe.transform(
                this.start_date,
                'dd-MM-yyyy'
            );
            const end_date = this.datePipe.transform(
                this.end_date,
                'dd-MM-yyyy'
            );
            dateFilter = `membership_start_date=${start_date}&membership_end_date=${end_date}&isActive=0`;
        }
        this.filteredMembershipData = this.apiService
            .getTypeRequest(`filtered_membership_data?${dateFilter}`)
            .pipe(
                map((response: any) => {
                    this.filteredMembershipDataLoading = false;
                    if (response.result) {
                        return response.data;
                    } else {
                        return null;
                    }
                })
            );
    }

    carryForwardConfig(product:filteredMembershipData){
        const ref = this.dialogService.open(CarryForwardConfigComponent,{
            data: product,
            header: `Configure`,
            styleClass: 'w-10 sm:w-10 md:w-10 lg:w-8',
        })
        ref.onClose.subscribe((result: any) => {
            if (result) {
                this.ngOnInit();
            }
        });
    }
}
