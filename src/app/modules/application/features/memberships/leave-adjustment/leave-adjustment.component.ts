import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subject, map } from 'rxjs';
import { filteredMembershipData } from '../carry-forward/filteredMembershipData';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { ApiService } from 'src/app/core/services/api/api.service';
import { MemberService } from '../../../core/services/MemberService/member.service';
import { LeaveAdjustmentConfigComponent } from './leave-adjustment-config/leave-adjustment-config.component';

@Component({
    selector: 'app-leave-adjustment',
    templateUrl: './leave-adjustment.component.html',
    styleUrls: ['./leave-adjustment.component.scss'],
})
export class LeaveAdjustmentComponent implements OnInit, OnDestroy {
    start_date: any;
    end_date: any;
    datePipe: DatePipe = new DatePipe('en-US');
    filteredMembershipData: Observable<filteredMembershipData[]>;
    filteredMembershipDataLoading: boolean = false;
    // Private
    private _unsubscribeAll: Subject<any> = new Subject<any>();
    constructor(
        public apiService: ApiService,
        private confirmationService: ConfirmationService,
        public messageService: MessageService,
        public dialogService: DialogService,
        public route: ActivatedRoute,
        public memberService: MemberService,
        public router: Router
    ) {}

    ngOnInit(): void {
        var userData = this.memberService.getUserData();
        var transaction_range = userData?.settings[1]?.settings_value ?? 30;
        this.end_date = new Date().toISOString().substring(0, 10);
        this.start_date = this.datePipe.transform(
            new Date().setDate(new Date().getDate() - transaction_range),
            'yyyy-MM-dd'
        );
        this.getFilteredMembershipData();
    }
    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }

    getFilteredMembershipData() {
        this.filteredMembershipDataLoading = true;
        var dateFilter = ``;
        if (this.start_date != null && this.end_date != null) {
            const start_date = this.datePipe.transform(
                this.start_date,
                'dd-MM-yyyy'
            );
            const end_date = this.datePipe.transform(
                this.end_date,
                'dd-MM-yyyy'
            );
            dateFilter = `membership_start_date=${start_date}&membership_end_date=${end_date}&isActive=1`;
        }
        this.filteredMembershipData = this.apiService
            .getTypeRequest(`filtered_membership_data?${dateFilter}`)
            .pipe(
                map((response: any) => {
                    this.filteredMembershipDataLoading = false;
                    if (response.result) {
                        return response.data;
                    } else {
                        return null;
                    }
                })
            );
    }

    leaveAdjustmentConfig(product: filteredMembershipData) {
        const ref = this.dialogService.open(LeaveAdjustmentConfigComponent, {
            data: product,
            header: `Configure`,
            styleClass: 'w-10 sm:w-10 md:w-10 lg:w-8',
        });
        ref.onClose.subscribe((result: any) => {
            if (result) {
                this.ngOnInit();
            }
        });
    }
}
