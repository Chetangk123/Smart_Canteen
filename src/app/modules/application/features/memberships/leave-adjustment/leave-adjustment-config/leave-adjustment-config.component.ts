import { Component, OnDestroy, OnInit } from '@angular/core';
import { MemberData } from '../../carry-forward/filteredMembershipData';
import { MessageService } from 'primeng/api';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { ApiService } from 'src/app/core/services/api/api.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subject, takeUntil } from 'rxjs';

@Component({
    selector: 'app-leave-adjustment-config',
    templateUrl: './leave-adjustment-config.component.html',
    styleUrls: ['./leave-adjustment-config.component.scss'],
})
export class LeaveAdjustmentConfigComponent implements OnInit, OnDestroy {
    membersToAddList: MemberData[];
    adjustment_days: any;
    commonForm: FormGroup = new FormGroup({
        adjustment_days: new FormControl('', [Validators.required]),
    });
    loading: boolean = false;
    // Private
    private _unsubscribeAll: Subject<any> = new Subject<any>();
    constructor(
        public ref: DynamicDialogRef,
        public apiService: ApiService,
        public config: DynamicDialogConfig,
        public messageService: MessageService
    ) {}

    ngOnInit(): void {
        this.membersToAddList = this.config.data.member_data;
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }

    submit() {
        if (this.commonForm.valid) {
            this.loading = true;
            var membersList: any[] = [];
            this.membersToAddList.forEach((member: any) => {
                membersList.push(Number(member.member_id));
            });
            var payload = {
                member_ids: membersList,
                adjustment_days: this.commonForm.controls.adjustment_days.value,
                isExtend: 0,
            };
            this.apiService
                .postTypeRequest('membership_date_adjustment', payload)
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe((response: any) => {
                    this.loading = false;
                    if (response.result) {
                        this.commonForm.reset();
                        this.membersToAddList = [];
                        this.messageService.add({
                            severity: 'success',
                            summary: 'Leave Adjustment',
                            detail: response.message,
                        });
                        this.ref.close(true);
                    } else {
                        this.messageService.add({
                            severity: 'error',
                            summary: 'Error',
                            detail: response.message,
                        });
                    }
                });
        } else {
            var controls = this.commonForm.controls;
            for (const name in controls) {
                controls[name].markAsDirty();
                controls[name].markAllAsTouched();
            }
        }
    }
}
