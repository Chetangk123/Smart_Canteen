import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-transactions',
    templateUrl: './transactions.component.html',
    styleUrls: ['./transactions.component.scss'],
})
export class TransactionsComponent implements OnInit {

    constructor(
    ) {}

    ngOnInit() {

    }

    // ConvertStringToNumber(input: string) {
    //     var numeric = Number(input);
    //     return numeric;
    // }

    // loadData() {
    //     var url = '';
    //     if (this.counter_id != '') {
    //         url = `/BY_COUNTER/${this.counter_id}`;
    //     }
    //     this.loading = true;
    //     var Data = {
    //         member_id: '',
    //         txn_id: '',
    //         account_id: '',
    //         start_date: this.start_date,
    //         end_date: this.end_date,
    //     };
    //     this.apiService
    //         .postTypeRequest(`transactions/RECENT${url}`, Data)
    //         .toPromise()
    //         .then((result: any) => {
    //             this.loading = false;
    //             if (result.result) {
    //                 this.Data = result.data;
    //             }
    //         })
    //         .finally(() => {
    //             this.loading = false;
    //         });
    // }

    // accountTransfer() {
    //     var sourceUrl = 'INCOME_ACCOUNT_HEAD';
    //     var destinationUrl = 'EXPENSE_ACCOUNT_HEAD';

    //     const ref = this.dialogService.open(AccountTransferComponent, {
    //         data: {
    //             sourceUrl: sourceUrl,
    //             destinationUrl: destinationUrl,
    //             url: 'account_head_txn/ACC_HEAD_EXPENSE',
    //         },
    //         header: `Expense Entry`,
    //         width: '60%',
    //     });
    //     ref.onClose.subscribe((result: any) => {
    //         if (result) {
    //             this.ngOnInit();
    //         }
    //     });
    // }

    // clear(table: Table) {
    //     table.clear();
    // }

    // add() {
    //     var newMembershipType: any;
    //     const ref = this.dialogService.open(AddEditTransactionComponent, {
    //         data: newMembershipType,
    //         header: `Transaction Details`,
    //         styleClass: 'w-10 sm:w-10 md:w-10 lg:w-5',
    //     });
    //     ref.onClose.subscribe((result: any) => {
    //         if (result) {
    //             this.ngOnInit();
    //         }
    //     });
    // }

    // view() {
    //     //
    //     this.displayTransaction = true;
    // }

    // print() {
    //     var Data = {
    //         full_name: this.selectedProduct.full_name,
    //         card_number: this.selectedProduct.card_number,
    //     };
    //     if (Data.card_number) {
    //         this.dialogService.open(TransactionReceiptComponent, {
    //             data: { txnData: this.selectedProduct, memberData: Data },
    //             header: `Transaction Details`,
    //             styleClass: 'w-8  xs:w-12 sm:w-12 md:w-10 lg:w-5',
    //         });
    //     } else {
    //         this.dialogService.open(ExpenseReceiptComponent, {
    //             data: { txnData: this.selectedProduct },
    //             header: `Transaction Details`,
    //             styleClass: 'w-8  xs:w-12 sm:w-12 md:w-10 lg:w-5',
    //         });
    //     }
    // }
}
