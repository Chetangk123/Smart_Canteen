(function (window) {
    window.__env = window.__env || {};
    document.querySelector("title").innerHTML = "Campus Bites";
    window.__env.config = {
        app: {
            appName: "Campus Bites",
            appLogoImage: "assets/logo.jpg",
            cardNumberLength: 14,
        },
        layout: {
            enableLocalStorage: true,
        },
        url: "https://thetechvaidya.com/cooksbook_new/api/v1/",
    };
    window.__env.contactDetails = {
        phoneNo: '+91-8056732355', // +91-xxxxxxxxxx
        name: "Sandesh S Dandin",
        address:`Bagalkot,
         Karnataka - 560033`
    }
})(this);
